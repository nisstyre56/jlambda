This is the beginnings of a little language based on a typed LC with let
and mainly inspired by these: 
  * http://cs.brown.edu/~sk/Publications/Books/ProgLangs/
  * http://www.cs.indiana.edu/hyplan/dfried/ts.ps (I plan on generating code in
    trampolined style)
  * http://lucacardelli.name/papers/basictypechecking.a4.pdf
